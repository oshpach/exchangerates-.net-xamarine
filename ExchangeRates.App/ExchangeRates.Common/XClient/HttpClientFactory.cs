﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using ExchangeRates.Common.Definitions;

namespace ExchangeRates.Common.XClient
{
	internal static class HttpClientFactory
	{
		public static HttpClient GetHttpClient(string baseUrl = null, string token = null)
		{
			var client = new HttpClient();

			if (!string.IsNullOrEmpty(baseUrl))
			{
				client.BaseAddress = new Uri(baseUrl);
			}

			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.Type.TypeJson));

			if (!string.IsNullOrEmpty(token))
			{
				client.DefaultRequestHeaders.Add("Authorization", string.Concat("Bearer ", token));
			}

			return client;
		}
	}
}

