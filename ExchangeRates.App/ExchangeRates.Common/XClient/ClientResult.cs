﻿using System.Net;

namespace ExchangeRates.Common.XClient
{
	public class ClientResult
	{
		public bool IsSuccess { get; set; }
		public string Message { get; set; }
		public HttpStatusCode StatusCode { get; set; }
	}

	public class ClientResult<T> : ClientResult where T : class
	{
		public T Obj { get; set; }
	}
}
