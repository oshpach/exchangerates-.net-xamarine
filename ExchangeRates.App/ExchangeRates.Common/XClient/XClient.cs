﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates.Common.Definitions;
using ExchangeRates.Common.Extensions;
using Newtonsoft.Json;

namespace ExchangeRates.Common.XClient
{
	public class XClient
	{
		private readonly string _baseUrl;
		private string _token;

		public XClient(string baseUrl, string token = null)
		{
			_baseUrl = baseUrl;
			_token = token;
		}

		public async Task<ClientResult> PostAsync(string url, object model)
		{
			var result = new ClientResult();

			try
			{
				using (var client = HttpClientFactory.GetHttpClient(_baseUrl, _token))
				{
					var postBody = JsonConvert.SerializeObject(model);
					var data = new StringContent(postBody, Encoding.UTF8, Constants.Type.TypeJson);
					var response = await client.PostAsync(url, data);

					if (response.IsSuccessStatusCode)
					{
						result.Message = await response.Content.ReadAsStringAsync();
						result.IsSuccess = true;
					}
					else
						result.Message = await response.GetErrorResponse();
				}
			}
			catch (Exception e)
			{
				result.Message = e.Message;
			}

			return result;
		}

		public async Task<ClientResult<T>> PostAsync<T>(string url, object model) where T : class
		{
			var result = new ClientResult<T>();

			try
			{
				using (var client = HttpClientFactory.GetHttpClient(_baseUrl, _token))
				{
					var postBody = JsonConvert.SerializeObject(model);
					var data = new StringContent(postBody, Encoding.UTF8, Constants.Type.TypeJson);
					var response = await client.PostAsync(url, data);

					if (response.IsSuccessStatusCode)
					{
						result.Obj = await response.Content.ReadAs<T>();
						result.IsSuccess = true;
					}
					else
						result.Message = await response.GetErrorResponse();
				}
			}
			catch (Exception e)
			{
				result.Message = e.Message;
			}

			return result;
		}

		public async Task<ClientResult<T>> GetAsync<T>(string url, object model) where T : class
		{
			var result = new ClientResult<T>();

			try
			{
				using (var client = HttpClientFactory.GetHttpClient(_baseUrl, _token))
				{
					var response = await client.GetAsync(url);

					if (response.IsSuccessStatusCode)
					{
						result.Obj = await response.Content.ReadAs<T>();
						result.IsSuccess = true;
					}
					else
						result.Message = await response.GetErrorResponse();
				}
			}
			catch (Exception e)
			{
				result.Message = e.Message;
			}

			return result;
		}

		public async Task<ClientResult<ObservableCollection<T>>> GetCollectionAsync<T>(string url, Dictionary<string, object> args = null) where T : class
		{
			var result = new ClientResult<ObservableCollection<T>>();

			try
			{
				using (var client = HttpClientFactory.GetHttpClient(_baseUrl, _token))
				{
					if (args != null && args.Any())
					{
						var builder = new StringBuilder($"{url}?");

						foreach (var arg in args)
						{
							builder.Append(arg.Key);
							builder.Append("=");
							builder.Append(arg.Value);

							if (!args.Last().Equals(arg))
							{
								builder.Append("&");
							}
						}

						url = builder.ToString();
					}

					var response = await client.GetAsync(url);

					result.StatusCode = response.StatusCode;

					if (response.IsSuccessStatusCode)
					{
						result.Obj = await response.Content.ReadAs<ObservableCollection<T>>();
						result.IsSuccess = true;
					}
					else
						result.Message = await response.GetErrorResponse();
				}
			}
			catch (Exception e)
			{
				result.Message = e.Message;
			}

			return result;
		}

		public void SetToken(string token)
		{
			_token = token;
		}
	}
}
