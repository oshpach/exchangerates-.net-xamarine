﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ExchangeRates.Common.Extensions
{
	public static class JsonHelper
	{
		public static async Task<T> ReadAs<T>(this HttpContent content)
		{
			if (content == null)
			{
				throw new NullReferenceException("HttpContent is NULL.");
			}

			using (var stream = new MemoryStream())
			{
				await content.CopyToAsync(stream);

				stream.Seek(0, SeekOrigin.Begin);

				var array = stream.ToArray();

				var result = System.Text.Encoding.UTF8.GetString(array, 0, array.Length);

				return JsonConvert.DeserializeObject<T>(result);
			}
		}
	}
}
