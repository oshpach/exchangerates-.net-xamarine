﻿using System.Net.Http;
using System.Threading.Tasks;
using ExchangeRates.Common.Definitions;

namespace ExchangeRates.Common.Extensions
{
	public static class HttpResponseMessageExtensions
	{
		public static async Task<string> GetErrorResponse(this HttpResponseMessage response)
		{
			var errorResult = await response.Content.ReadAs<ErrorResult>();

			var message = errorResult?.ResultObject?.Message;

			return message;
		}
	}
}

