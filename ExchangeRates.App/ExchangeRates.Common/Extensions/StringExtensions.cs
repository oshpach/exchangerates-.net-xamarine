﻿using System;

namespace ExchangeRates.Common.Extensions
{
	public static class StringExtensions
	{
		public static string LocalizeDate(this string date, string pattern = null, string locale = null)
		{
			var culture = new System.Globalization.CultureInfo(locale ?? "sv-SE");
			var format = pattern ?? "dd MMMMM yyyy hh:mm";
			var parsedDate = DateTime.Parse(date);

			return parsedDate.ToString(format, culture);
		}
	}
}

