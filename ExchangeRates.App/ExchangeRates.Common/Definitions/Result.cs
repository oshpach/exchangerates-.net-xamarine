﻿namespace ExchangeRates.Common.Definitions
{
    public class Result
    {
        public Result()
        {
            Status = ResultStatus.Error;
        }

        public ResultStatus Status { get; set; }
        public object Obj { get; set; }
        public string Message { get; set; }
    }

    public class Result<T> where T : class
    {
        public Result()
        {
            Status = ResultStatus.Error;
        }

        public ResultStatus Status { get; set; }
        public T Obj { get; set; }
        public string Message { get; set; }
    }
}
