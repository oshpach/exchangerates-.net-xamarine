﻿namespace ExchangeRates.Common.Definitions
{
    public static class Message
    {
        public const string ErrorWhileLoadingData = "Error occured while loading data.";
    }
}
