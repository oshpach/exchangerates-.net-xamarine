﻿namespace ExchangeRates.Common.Definitions
{
    public static class Constants
    {
        public static class Url
        {
            //public static string ExchangeRateBaseUrl = "http://localhost:40469/api/";
            public static string ExchangeRateBaseUrl = "http://exchangerates.dev.empeek.net/api/";

            public static class ExchangeRate
            {
                public static string BaseUrl = "ExchangeRates/";

                public static string ExchangeRates = BaseUrl + "ExchangeRates";
            }
        }
        public static class Type
        {
            public const string TypeJson = "application/json";
        }
    }
}
