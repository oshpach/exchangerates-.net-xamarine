﻿namespace ExchangeRates.Common.Definitions
{
    public class ServiceResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }

    public class ServiceResponse<T> : ServiceResponse where T : class
    {
        public T Obj { get; set; }
    }
}
