﻿namespace ExchangeRates.Common.Definitions
{
    public class ErrorResult
    {
        public string Process { get; set; }
        public ServiceResponse ResultObject { get; set; }
        public object Params { get; set; }
        public object Response { get; set; }
    }
}
