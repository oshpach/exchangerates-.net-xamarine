﻿namespace ExchangeRates.Common.Definitions
{
    public enum ResultStatus
    {
        Ok,
        Error
    }

    public enum ExchangeRatesType
    {
        NBU = 1,
        BlackMarket = 2
    }

    public enum ExchangeRatesСurrency
    {
        USD = 1,
        EUR = 2
    }
}
