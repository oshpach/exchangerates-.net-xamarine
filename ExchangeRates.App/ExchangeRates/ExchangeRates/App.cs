﻿using ExchangeRates.Views;
using Xamarin.Forms;

namespace ExchangeRates
{
    public class App : Application
    {
        public App()
        {
            MainPage = new ExchangeRatePage();
        }
    }
}
