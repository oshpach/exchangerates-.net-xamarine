﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ExchangeRates.Common.Definitions;
using ExchangeRates.Models;

namespace ExchangeRates.Core.Service
{
    interface IExchangeRateService
    {
        Task<Result<ObservableCollection<ExchangeRateModel>>> GetExchangeRates();
    }
}
