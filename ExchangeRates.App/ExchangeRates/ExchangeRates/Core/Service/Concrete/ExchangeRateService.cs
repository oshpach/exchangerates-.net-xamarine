﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ExchangeRates.Common.Definitions;
using ExchangeRates.Common.XClient;
using ExchangeRates.Core.Service.Concrete;
using ExchangeRates.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof (ExchangeRateService))]

namespace ExchangeRates.Core.Service.Concrete
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly XClient _client;

        public ExchangeRateService()
        {
            _client = new XClient(Constants.Url.ExchangeRateBaseUrl);
        }

        public async Task<Result<ObservableCollection<ExchangeRateModel>>> GetExchangeRates()
        {
            var result = new Result<ObservableCollection<ExchangeRateModel>>();

            var response = await _client.GetCollectionAsync<ExchangeRateModel>(Constants.Url.ExchangeRate.ExchangeRates);

            if (response.IsSuccess)
            {
                result.Obj = response.Obj;

                result.Status = ResultStatus.Ok;
            }
            else
            {
                result.Message = Message.ErrorWhileLoadingData;
            }

            return result;
        }
    }
}