﻿using System.Collections.ObjectModel;
using System.Linq;
using ExchangeRates.Common.Definitions;
using ExchangeRates.Core.Service;
using ExchangeRates.Models;
using Xamarin.Forms;

namespace ExchangeRates.ViewModels
{
    public class ExchangeRateViewModel : BaseViewModel
    {
        private readonly IExchangeRateService _exchangeRateService;

        private ObservableCollection<ExchangeRateModel> _blackMarketRates;
        private ObservableCollection<ExchangeRateModel> _nbuRates;

        public ObservableCollection<ExchangeRateModel> NbuRates
        {
            get { return _nbuRates; }
            set { SetProperty(ref _nbuRates, value); }
        }

        public ObservableCollection<ExchangeRateModel> BlackMarketRates
        {
            get { return _blackMarketRates; }
            set { SetProperty(ref _blackMarketRates, value); }
        }

        public ExchangeRateViewModel()
        {
            _exchangeRateService = DependencyService.Get<IExchangeRateService>();
            _nbuRates = new ObservableCollection<ExchangeRateModel>();
            _blackMarketRates=new ObservableCollection<ExchangeRateModel>();

            GetExchangeRates();
        }

        public async void GetExchangeRates()
        {
            var result = await _exchangeRateService.GetExchangeRates();

            if (result.Status == ResultStatus.Ok)
            {
                NbuRates = new ObservableCollection<ExchangeRateModel>
                    (result.Obj.Where(r => r.Type == ExchangeRatesType.NBU));

                BlackMarketRates = new ObservableCollection<ExchangeRateModel>
                    (result.Obj.Where(r => r.Type == ExchangeRatesType.BlackMarket));
            }
        }
    }
}