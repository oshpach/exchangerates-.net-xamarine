﻿using ExchangeRates.Common.Definitions;

namespace ExchangeRates.Models
{
    public class ExchangeRateModel
    {
        public decimal BuyValue { get; set; }
        public decimal SellValue { get; set; }
        public ExchangeRatesType Type { get; set; }
        public ExchangeRatesСurrency Currency { get; set; }
    }
}