﻿using ExchangeRates.ViewModels;
using Xamarin.Forms;

namespace ExchangeRates.Views
{
    public partial class ExchangeRatePage : ContentPage
    {
        private readonly ExchangeRateViewModel _model;
        public ExchangeRatePage()
        {
            BindingContext = _model ?? (_model = new ExchangeRateViewModel());
            InitializeComponent();
        }
    }
}
