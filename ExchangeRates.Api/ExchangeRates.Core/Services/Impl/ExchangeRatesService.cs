﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using ExchangeRates.Common.Enums;
using ExchangeRates.Common.Models;
using ExchangeRates.Common.Settings;
using HtmlAgilityPack;

namespace ExchangeRates.Core.Services.Impl
{
    public class ExchangeRatesService : IExchangeRatesService
    {
        public IEnumerable<ExchangeRatesModel> GetExchangeRates()
        {
            try
            {
                var result = new List<ExchangeRatesModel>();

                result.AddRange(GetFinanceIUaNBUExchangeRates());
                result.AddRange(GetFinanceIUaBlackMarketExchangeRates());

                return result;
            }
            catch (Exception)
            {
                //Ignore.
            }

            return new List<ExchangeRatesModel>();
        }

        private IEnumerable<ExchangeRatesModel> GetFinanceIUaBlackMarketExchangeRates()
        {
            try
            {
                using (var client = new WebClient { Encoding = Encoding.UTF8 })
                {
                    var nbuExchangeRatesData = client.DownloadString(FinanceIUaSettings.BlackMarketParseUrl);

                    if (!string.IsNullOrWhiteSpace(nbuExchangeRatesData))
                    {
                        var htmlDocNewses = new HtmlDocument();

                        htmlDocNewses.LoadHtml(nbuExchangeRatesData);

                        var exchangeRatesTable = htmlDocNewses.DocumentNode.Descendants("table")
                            .Where(d => d.Attributes.Contains("class") &&
                                d.Attributes["class"].Value.Contains("local_table"))
                                .ToList()[1];

                        var exchangeRates = exchangeRatesTable.SelectNodes("tr")
                            .Where(p => p.ChildNodes
                                .Any(s => 
                                    string.Equals(s.InnerText, ExchangeRatesСurrenciesEnum.USD.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
                                    string.Equals(s.InnerText, ExchangeRatesСurrenciesEnum.EUR.ToString(), StringComparison.CurrentCultureIgnoreCase)))
                                    .ToList();

                        var result = new List<ExchangeRatesModel>();

                        foreach (var e in exchangeRates)
                        {
                            try
                            {
                                var currency = e.SelectNodes("td")[0].InnerText;
                                var buyPrice = e.SelectNodes("td")[1].SelectNodes("big")[0].InnerText;
                                var sellPrice = e.SelectNodes("td")[2].SelectNodes("big")[0].InnerText;

                                var exchangeRate = new ExchangeRatesModel
                                {
                                    Type = ExchangeRatesTypesEnum.BlackMarket,
                                    Currency = (ExchangeRatesСurrenciesEnum)Enum.Parse(typeof(ExchangeRatesСurrenciesEnum), currency, true),
                                    SellValue = decimal.Parse(sellPrice),
                                    BuyValue = decimal.Parse(buyPrice)
                                };

                                result.Add(exchangeRate);
                            }
                            catch (Exception)
                            {
                                //Ignore.
                            }
                        }

                        return result;
                    }
                }
            }
            catch (Exception)
            {
                //Ignore.
            }

            return new List<ExchangeRatesModel>();
        }

        private IEnumerable<ExchangeRatesModel> GetFinanceIUaNBUExchangeRates()
        {
            try
            {
                using (var client = new WebClient { Encoding = Encoding.UTF8 })
                {
                    var nbuExchangeRatesData = client.DownloadString(FinanceIUaSettings.NBUParseUrl);

                    if (!string.IsNullOrWhiteSpace(nbuExchangeRatesData))
                    {
                        var htmlDocNewses = new HtmlDocument();

                        htmlDocNewses.LoadHtml(nbuExchangeRatesData);

                        var exchangeRatesTable = htmlDocNewses.DocumentNode.Descendants("table")
                            .Where(d => d.Attributes.Contains("class") &&
                                d.Attributes["class"].Value.Contains("local_table"))
                                .ToList()[2];

                        var exchangeRates = exchangeRatesTable.SelectNodes("tr")
                            .Where(p => p.ChildNodes
                                .Any(s =>
                                    string.Equals(s.InnerText, ExchangeRatesСurrenciesEnum.USD.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
                                    string.Equals(s.InnerText, ExchangeRatesСurrenciesEnum.EUR.ToString(), StringComparison.CurrentCultureIgnoreCase)))
                                    .ToList();

                        var result = new List<ExchangeRatesModel>();

                        foreach (var e in exchangeRates)
                        {
                            try
                            {
                                var currency = e.SelectNodes("td")[0].InnerText;
                                var buyPrice = e.SelectNodes("td")[1].SelectNodes("big")[0].InnerText;
                                var sellPrice = e.SelectNodes("td")[2].SelectNodes("big")[0].InnerText;

                                var exchangeRate = new ExchangeRatesModel
                                {
                                    Type = ExchangeRatesTypesEnum.NBU,
                                    Currency = (ExchangeRatesСurrenciesEnum)Enum.Parse(typeof(ExchangeRatesСurrenciesEnum), currency, true),
                                    SellValue = decimal.Parse(sellPrice),
                                    BuyValue = decimal.Parse(buyPrice)
                                };

                                result.Add(exchangeRate);
                            }
                            catch (Exception)
                            {
                                //Ignore.
                            }
                        }

                        return result;
                    }
                }
            }
            catch (Exception)
            {
                //Ignore.
            }

            return new List<ExchangeRatesModel>();
        }
    }
}