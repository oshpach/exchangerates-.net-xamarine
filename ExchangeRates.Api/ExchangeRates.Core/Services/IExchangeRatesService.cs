﻿using System.Collections.Generic;
using ExchangeRates.Common.DR.Models;
using ExchangeRates.Common.Models;

namespace ExchangeRates.Core.Services
{
    public interface IExchangeRatesService : IDependency
    {
        IEnumerable<ExchangeRatesModel> GetExchangeRates();
    }
}