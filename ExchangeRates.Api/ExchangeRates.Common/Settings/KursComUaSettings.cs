﻿namespace ExchangeRates.Common.Settings
{
    public static class KursComUaSettings
    {
        public static string NBUParseUrl = "http://kurs.com.ua/";
        public static string BlackMarketParseUrl = "http://kurs.com.ua/blackmarket";
    }
}