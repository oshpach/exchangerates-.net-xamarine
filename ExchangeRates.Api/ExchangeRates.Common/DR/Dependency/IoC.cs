using System;
using StructureMap;

namespace ExchangeRates.Common.DR.Dependency
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            var container = Ioc.Instance;
            var dirToScan = AppDomain.CurrentDomain.BaseDirectory + "bin\\";

            container.Configure(c =>
            {
                c.AddRegistry<DefaultRegistry>();
                c.AddRegistry(new IocScanerRegistry(dirToScan));
            });

            return container;
        }
    }
}