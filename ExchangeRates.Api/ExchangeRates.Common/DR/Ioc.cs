using System;
using StructureMap;

namespace ExchangeRates.Common.DR
{
    public class Ioc
    {
        public static IContainer Container;
        public static object CreateInstanceLocker = new object();

        public static IContainer Instance
        {
            get
            {
                if (Container == null)
                {
                    lock (CreateInstanceLocker)
                    {
                        if (Container == null)
                            Container = new Container();
                    }
                }

                return Container;
            }
        }

        public static T Get<T>()
        {
            return Instance.GetInstance<T>();
        }

        public static object Get(Type type)
        {
            return Instance.GetInstance(type);
        }

        public static void Add(Action<ConfigurationExpression> exp)
        {
            Instance.Configure(exp);
        }
    }
}