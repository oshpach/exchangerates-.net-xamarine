﻿using System;
using System.Linq;
using ExchangeRates.Common.DR.Models;
using StructureMap;

namespace ExchangeRates.Common.DR
{
    public class IocScanerRegistry : Registry
    {
        public IocScanerRegistry(string pathToScan)
        {
            Scan(scan =>
            {
                scan.AssembliesFromPath(pathToScan, asmbl => asmbl.GetName().Name.Contains("ExchangeRates."));
                scan.Exclude(t => !HasDefaultConstructor(t));
                scan.WithDefaultConventions();
            });
        }

        private static bool HasDefaultConstructor(Type type)
        {
            if (type.GetInterfaces().ToList().Find(x => x.FullName == typeof(IDependency).FullName) != null)
            {
                return true;
            }

            return false;
        }
    }
}