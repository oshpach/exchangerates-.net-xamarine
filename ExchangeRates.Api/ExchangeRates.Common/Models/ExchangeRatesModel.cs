﻿using ExchangeRates.Common.Enums;

namespace ExchangeRates.Common.Models
{
    public class ExchangeRatesModel
    {
        public decimal BuyValue { get; set; }
        public decimal SellValue { get; set; }
        public ExchangeRatesTypesEnum Type { get; set; }
        public ExchangeRatesСurrenciesEnum Currency { get; set; }
    }
}