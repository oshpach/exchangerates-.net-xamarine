﻿using System.Web.Http;
using ExchangeRates.Core.Services;

namespace ExchangeRates.Api.Controllers
{
    [RoutePrefix("api/ExchangeRates")]
    public class ExchangeRatesController : BaseApiController
    {
        private readonly IExchangeRatesService _exchangeRatesService;

        public ExchangeRatesController(IExchangeRatesService exchangeRatesService)
        {
            _exchangeRatesService = exchangeRatesService;
        }

        [HttpGet]
        [Route("ExchangeRates")]
        public IHttpActionResult ExchangeRates()
        {
            return Ok(_exchangeRatesService.GetExchangeRates());
        }
    }
}