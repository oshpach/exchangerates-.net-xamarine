using System.Web;
using System.Web.Http;
using ExchangeRates.Api;
using ExchangeRates.Common.DR.Dependency;

[assembly: PreApplicationStartMethod(typeof(StructuremapMvc), "Start")]

namespace ExchangeRates.Api
{
    public static class StructuremapMvc
    {
        public static void Start()
        {
            var container = IoC.Initialize();

            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}